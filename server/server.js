"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var express = require("express");
var Server = /** @class */ (function () {
    function Server() {
        this.app = express();
    }
    Server.prototype.setRoutes = function () {
        this.app.get('/', this.renderIndex);
    };
    Server.prototype.startServer = function () {
        this.app.listen(3000, function () {
            console.log('Application listening on port 3000 ');
        });
    };
    Server.prototype.renderIndex = function (req, res) {
        res.send('Hello World !!!');
    };
    return Server;
}());
exports.Server = Server;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNlcnZlci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLGlDQUFtQztBQUVuQztJQUVJO1FBQ0ksSUFBSSxDQUFDLEdBQUcsR0FBRyxPQUFPLEVBQUUsQ0FBQztJQUN6QixDQUFDO0lBRU0sMEJBQVMsR0FBaEI7UUFDSSxJQUFJLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxHQUFHLEVBQUUsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDO0lBQ3hDLENBQUM7SUFFTSw0QkFBVyxHQUFsQjtRQUNJLElBQUksQ0FBQyxHQUFHLENBQUMsTUFBTSxDQUFDLElBQUksRUFBRTtZQUNsQixPQUFPLENBQUMsR0FBRyxDQUFDLHFDQUFxQyxDQUFDLENBQUM7UUFDdkQsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRU8sNEJBQVcsR0FBbkIsVUFBb0IsR0FBb0IsRUFBRSxHQUFxQjtRQUMzRCxHQUFHLENBQUMsSUFBSSxDQUFDLGlCQUFpQixDQUFDLENBQUM7SUFDaEMsQ0FBQztJQUNMLGFBQUM7QUFBRCxDQW5CQSxBQW1CQyxJQUFBO0FBbkJZLHdCQUFNIiwiZmlsZSI6InNlcnZlci5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCAqIGFzIGV4cHJlc3MgZnJvbSAnZXhwcmVzcyc7XHJcblxyXG5leHBvcnQgY2xhc3MgU2VydmVyIHtcclxuICAgIHByaXZhdGUgYXBwOiBleHByZXNzLkV4cHJlc3M7XHJcbiAgICBjb25zdHJ1Y3RvcigpIHtcclxuICAgICAgICB0aGlzLmFwcCA9IGV4cHJlc3MoKTtcclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgc2V0Um91dGVzKCkge1xyXG4gICAgICAgIHRoaXMuYXBwLmdldCgnLycsIHRoaXMucmVuZGVySW5kZXgpO1xyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBzdGFydFNlcnZlcigpIHtcclxuICAgICAgICB0aGlzLmFwcC5saXN0ZW4oMzAwMCwgZnVuY3Rpb24oKXtcclxuICAgICAgICAgICAgY29uc29sZS5sb2coJ0FwcGxpY2F0aW9uIGxpc3RlbmluZyBvbiBwb3J0IDMwMDAgJyk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSByZW5kZXJJbmRleChyZXE6IGV4cHJlc3MuUmVxdWVzdCwgcmVzOiBleHByZXNzLlJlc3BvbnNlKSB7XHJcbiAgICAgICAgcmVzLnNlbmQoJ0hlbGxvIFdvcmxkICEhIScpO1xyXG4gICAgfVxyXG59Il19
