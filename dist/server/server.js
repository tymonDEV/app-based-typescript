"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var express = require("express");
var bodyParser = require("body-parser");
var IndexRoute = require("./routes/index");
var ProductsRoute = require("./routes/products");
var Server = /** @class */ (function () {
    function Server() {
        this.app = express();
        this.app.use(bodyParser.json());
        this.app.use(bodyParser.urlencoded({
            extended: true
        }));
        this.setRoutes();
    }
    Server.bootstrap = function () {
        return new Server();
    };
    Server.prototype.setRoutes = function () {
        var router = express.Router();
        router.use(IndexRoute.Index.routes());
        router.use(ProductsRoute.Products.routes());
        this.app.use(router);
    };
    Server.prototype.startServer = function () {
        this.app.listen(3000, function () {
            console.log('Application listening on port 3000 ');
        });
    };
    return Server;
}());
exports.Server = Server;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNlcnZlci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLGlDQUFtQztBQUNuQyx3Q0FBMEM7QUFDMUMsMkNBQTZDO0FBQzdDLGlEQUFtRDtBQUVuRDtJQU9JO1FBQ0ksSUFBSSxDQUFDLEdBQUcsR0FBRyxPQUFPLEVBQUUsQ0FBQztRQUNyQixJQUFJLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxVQUFVLENBQUMsSUFBSSxFQUFFLENBQUMsQ0FBQztRQUNoQyxJQUFJLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxVQUFVLENBQUMsVUFBVSxDQUFDO1lBQy9CLFFBQVEsRUFBRSxJQUFJO1NBQ2pCLENBQUMsQ0FBQyxDQUFDO1FBQ0osSUFBSSxDQUFDLFNBQVMsRUFBRSxDQUFDO0lBQ3JCLENBQUM7SUFYYSxnQkFBUyxHQUF2QjtRQUNJLE1BQU0sQ0FBQyxJQUFJLE1BQU0sRUFBRSxDQUFDO0lBQ3hCLENBQUM7SUFXTywwQkFBUyxHQUFqQjtRQUNJLElBQUksTUFBTSxHQUFtQixPQUFPLENBQUMsTUFBTSxFQUFFLENBQUM7UUFFOUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLE1BQU0sRUFBRSxDQUFDLENBQUM7UUFDdEMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxhQUFhLENBQUMsUUFBUSxDQUFDLE1BQU0sRUFBRSxDQUFDLENBQUM7UUFFNUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsTUFBTSxDQUFDLENBQUM7SUFDekIsQ0FBQztJQUVNLDRCQUFXLEdBQWxCO1FBQ0ksSUFBSSxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsSUFBSSxFQUFFO1lBQ2xCLE9BQU8sQ0FBQyxHQUFHLENBQUMscUNBQXFDLENBQUMsQ0FBQztRQUN2RCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFDTCxhQUFDO0FBQUQsQ0E5QkEsQUE4QkMsSUFBQTtBQTlCWSx3QkFBTSIsImZpbGUiOiJzZXJ2ZXIuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgKiBhcyBleHByZXNzIGZyb20gJ2V4cHJlc3MnO1xyXG5pbXBvcnQgKiBhcyBib2R5UGFyc2VyIGZyb20gJ2JvZHktcGFyc2VyJztcclxuaW1wb3J0ICogYXMgSW5kZXhSb3V0ZSBmcm9tIFwiLi9yb3V0ZXMvaW5kZXhcIjtcclxuaW1wb3J0ICogYXMgUHJvZHVjdHNSb3V0ZSBmcm9tIFwiLi9yb3V0ZXMvcHJvZHVjdHNcIjtcclxuXHJcbmV4cG9ydCBjbGFzcyBTZXJ2ZXIge1xyXG4gICAgcHJpdmF0ZSBhcHA6IGV4cHJlc3MuRXhwcmVzcztcclxuXHJcbiAgICBwdWJsaWMgc3RhdGljIGJvb3RzdHJhcCgpOiBTZXJ2ZXIge1xyXG4gICAgICAgIHJldHVybiBuZXcgU2VydmVyKCk7XHJcbiAgICB9XHJcblxyXG4gICAgY29uc3RydWN0b3IoKSB7XHJcbiAgICAgICAgdGhpcy5hcHAgPSBleHByZXNzKCk7XHJcbiAgICAgICAgdGhpcy5hcHAudXNlKGJvZHlQYXJzZXIuanNvbigpKTtcclxuICAgICAgICB0aGlzLmFwcC51c2UoYm9keVBhcnNlci51cmxlbmNvZGVkKHtcclxuICAgICAgICAgICAgZXh0ZW5kZWQ6IHRydWVcclxuICAgICAgICB9KSk7XHJcbiAgICAgICAgdGhpcy5zZXRSb3V0ZXMoKTtcclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIHNldFJvdXRlcygpIHtcclxuICAgICAgICBsZXQgcm91dGVyOiBleHByZXNzLlJvdXRlciA9IGV4cHJlc3MuUm91dGVyKCk7XHJcblxyXG4gICAgICAgIHJvdXRlci51c2UoSW5kZXhSb3V0ZS5JbmRleC5yb3V0ZXMoKSk7XHJcbiAgICAgICAgcm91dGVyLnVzZShQcm9kdWN0c1JvdXRlLlByb2R1Y3RzLnJvdXRlcygpKTtcclxuXHJcbiAgICAgICAgdGhpcy5hcHAudXNlKHJvdXRlcik7XHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIHN0YXJ0U2VydmVyKCkge1xyXG4gICAgICAgIHRoaXMuYXBwLmxpc3RlbigzMDAwLCBmdW5jdGlvbigpe1xyXG4gICAgICAgICAgICBjb25zb2xlLmxvZygnQXBwbGljYXRpb24gbGlzdGVuaW5nIG9uIHBvcnQgMzAwMCAnKTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxufSJdfQ==
