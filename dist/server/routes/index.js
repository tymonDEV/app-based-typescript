"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var express = require("express");
var Index = /** @class */ (function () {
    function Index() {
    }
    Index.routes = function () {
        var router = express.Router();
        var indexRoute = new Index();
        router.get('/', indexRoute.index.bind(indexRoute));
        return router;
    };
    Index.prototype.index = function (req, res) {
        res.send('Hello World !!!');
    };
    return Index;
}());
exports.Index = Index;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInJvdXRlcy9pbmRleC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLGlDQUFtQztBQUVuQztJQUFBO0lBY0EsQ0FBQztJQVppQixZQUFNLEdBQXBCO1FBQ0ksSUFBSSxNQUFNLEdBQW1CLE9BQU8sQ0FBQyxNQUFNLEVBQUUsQ0FBQztRQUM5QyxJQUFJLFVBQVUsR0FBVSxJQUFJLEtBQUssRUFBRSxDQUFDO1FBRXBDLE1BQU0sQ0FBQyxHQUFHLENBQUMsR0FBRyxFQUFFLFVBQVUsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUM7UUFFbkQsTUFBTSxDQUFDLE1BQU0sQ0FBQztJQUNsQixDQUFDO0lBRU0scUJBQUssR0FBWixVQUFhLEdBQW9CLEVBQUUsR0FBcUI7UUFDcEQsR0FBRyxDQUFDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDO0lBQ2hDLENBQUM7SUFDTCxZQUFDO0FBQUQsQ0FkQSxBQWNDLElBQUE7QUFkWSxzQkFBSyIsImZpbGUiOiJyb3V0ZXMvaW5kZXguanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgKiBhcyBleHByZXNzIGZyb20gJ2V4cHJlc3MnO1xyXG5cclxuZXhwb3J0IGNsYXNzIEluZGV4IHtcclxuICAgIFxyXG4gICAgcHVibGljIHN0YXRpYyByb3V0ZXMoKTogZXhwcmVzcy5Sb3V0ZXIge1xyXG4gICAgICAgIGxldCByb3V0ZXI6IGV4cHJlc3MuUm91dGVyID0gZXhwcmVzcy5Sb3V0ZXIoKTtcclxuICAgICAgICBsZXQgaW5kZXhSb3V0ZTogSW5kZXggPSBuZXcgSW5kZXgoKTtcclxuICAgICAgICBcclxuICAgICAgICByb3V0ZXIuZ2V0KCcvJywgaW5kZXhSb3V0ZS5pbmRleC5iaW5kKGluZGV4Um91dGUpKTtcclxuXHJcbiAgICAgICAgcmV0dXJuIHJvdXRlcjtcclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgaW5kZXgocmVxOiBleHByZXNzLlJlcXVlc3QsIHJlczogZXhwcmVzcy5SZXNwb25zZSkge1xyXG4gICAgICAgIHJlcy5zZW5kKCdIZWxsbyBXb3JsZCAhISEnKTtcclxuICAgIH1cclxufVxyXG5cclxuIl19
