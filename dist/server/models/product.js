"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Product = /** @class */ (function () {
    function Product(id, name, quantity) {
        if (quantity === void 0) { quantity = 0; }
        this.id = id;
        this.name = name;
        this.quantity = quantity;
        this.id = id;
    }
    Product.prototype.getId = function () {
        return this.id;
    };
    Product.prototype.getName = function () {
        return this.name;
    };
    Product.prototype.setName = function (newName) {
        this.name = newName;
    };
    Product.prototype.updateQuantity = function (newQuantity) {
        this.quantity = newQuantity;
    };
    Product.prototype.addProducts = function (addedQuantity) {
        this.quantity += addedQuantity;
    };
    Product.prototype.subtractQuantity = function (subtractedQuantity) {
        this.quantity = subtractedQuantity;
    };
    return Product;
}());
exports.Product = Product;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm1vZGVscy9wcm9kdWN0LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUE7SUFDSSxpQkFBbUIsRUFBVSxFQUFTLElBQVksRUFBUyxRQUFvQjtRQUFwQix5QkFBQSxFQUFBLFlBQW9CO1FBQTVELE9BQUUsR0FBRixFQUFFLENBQVE7UUFBUyxTQUFJLEdBQUosSUFBSSxDQUFRO1FBQVMsYUFBUSxHQUFSLFFBQVEsQ0FBWTtRQUMzRSxJQUFJLENBQUMsRUFBRSxHQUFHLEVBQUUsQ0FBQztJQUNqQixDQUFDO0lBRU0sdUJBQUssR0FBWjtRQUNJLE1BQU0sQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDO0lBQ25CLENBQUM7SUFFTSx5QkFBTyxHQUFkO1FBQ0ksTUFBTSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUM7SUFDckIsQ0FBQztJQUVNLHlCQUFPLEdBQWQsVUFBZSxPQUFlO1FBQzFCLElBQUksQ0FBQyxJQUFJLEdBQUcsT0FBTyxDQUFDO0lBQ3hCLENBQUM7SUFFTSxnQ0FBYyxHQUFyQixVQUFzQixXQUFtQjtRQUNyQyxJQUFJLENBQUMsUUFBUSxHQUFHLFdBQVcsQ0FBQztJQUNoQyxDQUFDO0lBRU0sNkJBQVcsR0FBbEIsVUFBbUIsYUFBcUI7UUFDcEMsSUFBSSxDQUFDLFFBQVEsSUFBSSxhQUFhLENBQUM7SUFDbkMsQ0FBQztJQUVNLGtDQUFnQixHQUF2QixVQUF3QixrQkFBMEI7UUFDOUMsSUFBSSxDQUFDLFFBQVEsR0FBRyxrQkFBa0IsQ0FBQztJQUN2QyxDQUFDO0lBRUwsY0FBQztBQUFELENBN0JBLEFBNkJDLElBQUE7QUE3QlksMEJBQU8iLCJmaWxlIjoibW9kZWxzL3Byb2R1Y3QuanMiLCJzb3VyY2VzQ29udGVudCI6WyJleHBvcnQgY2xhc3MgUHJvZHVjdCB7XHJcbiAgICBjb25zdHJ1Y3RvcihwdWJsaWMgaWQ6IG51bWJlciwgcHVibGljIG5hbWU6IHN0cmluZywgcHVibGljIHF1YW50aXR5OiBudW1iZXIgPSAwKSB7XHJcbiAgICAgICAgdGhpcy5pZCA9IGlkO1xyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBnZXRJZCgpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5pZDtcclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgZ2V0TmFtZSgpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5uYW1lO1xyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBzZXROYW1lKG5ld05hbWU6IHN0cmluZykge1xyXG4gICAgICAgIHRoaXMubmFtZSA9IG5ld05hbWU7XHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIHVwZGF0ZVF1YW50aXR5KG5ld1F1YW50aXR5OiBudW1iZXIpIHtcclxuICAgICAgICB0aGlzLnF1YW50aXR5ID0gbmV3UXVhbnRpdHk7XHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIGFkZFByb2R1Y3RzKGFkZGVkUXVhbnRpdHk6IG51bWJlcikge1xyXG4gICAgICAgIHRoaXMucXVhbnRpdHkgKz0gYWRkZWRRdWFudGl0eTtcclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgc3VidHJhY3RRdWFudGl0eShzdWJ0cmFjdGVkUXVhbnRpdHk6IG51bWJlcikge1xyXG4gICAgICAgIHRoaXMucXVhbnRpdHkgPSBzdWJ0cmFjdGVkUXVhbnRpdHk7XHJcbiAgICB9XHJcbiAgICBcclxufSJdfQ==
