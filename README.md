# typescript-brickbuster

## Setup
Instructions below will help you run project on your local machine.

### Download repository
You can download repository on github and set up your project then open project in terminal and type:
```
npm install
```
### Clone repository
```
git clone https://tymonDEV@bitbucket.org/tymonDEV/app-based-typescript.git your-app-name (or) . (this same folder in which you is)
cd your-app-name
rm -rf .git # remove git history
```
## Starting aplication
Go to your project directory and type in console:
```
gulp webserve
```