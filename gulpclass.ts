import { Gulpclass, Task, SequenceTask } from "gulpclass/Decorators";

const gulp = require('gulp');
const path = require('path');
const ts = require('gulp-typescript');
const sourcemaps = require('gulp-sourcemaps');
const del = require('del');

@Gulpclass()
export class Gulpfile {

    @Task()
    clean(cb: Function) {
        return del(['./dist/**'], cb)
    }
    
    @Task('build:server')
    buildServer() {
        let tsProject = ts.createProject(
            path.resolve('./server/tsconfig.json')
        );
        let tsResult = gulp.src(['./server/**/*.ts'])
            .pipe(sourcemaps.init())
            .pipe(tsProject());
        return tsResult.js
            .pipe(sourcemaps.write())
            .pipe(gulp.dest(
                path.resolve('./dist/server')
        ))
    }

    @Task('build:client')
    buildClient() {
        let tsProject = ts.createProject(
            path.resolve('./client/tsconfig.json')
        );
        let tsResult = gulp.src(['./client/**/*.ts'])
            .pipe(sourcemaps.init())
            .pipe(tsProject());
        return tsResult.js
            .pipe(sourcemaps.write())
            .pipe(gulp.dest(
                path.resolve('./dist/client')
        ))
    }

    @Task('copy:client')
    copyClient() {
        return gulp.src([
            './client/system.config.js',
            './client/index.html'
        ])
        .pipe(gulp.dest(
            path.resolve('./dist/client')
        ))
    }

    @SequenceTask()
    default() {
        return ['clean', 'build:server', 'build:client', 'copy:client'];
    }
}
